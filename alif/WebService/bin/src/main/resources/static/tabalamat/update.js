tabalamatid()

function tabalamatid() {
	let id = sessionStorage.getItem("id")
	$.ajax({
		url: "http://localhost:81/api/tabalamat/list/" + id,
		type: "GET",
		success: function(hasil) {
			$("#ilabel").val(hasil.label)
			$("#irecipient").val(hasil.recipient)
			$("#irecipientphonenumber").val(hasil.recipientphonenumber)
			$("#ilocationid").val(hasil.locationid)
			$("#ipostalcode").val(hasil.postalcode)
			$("#iaddress").val(hasil.address)
			console.log(hasil)
		}
	})
}

$(".btnsimpanupdate").click(function() {
	let id = sessionStorage.getItem("id")
	var obj = {}
	obj.id = id
	obj.label = $("#ilabel").val()
	obj.recipient = $("#irecipient").val()
	obj.recipientphonenumber = $("#irecipientphonenumber").val()
	obj.locationid = $("#ilocationid").val()
	obj.postalcode = $("#ipostalcode").val()
	obj.address = $("#iaddress").val()
	var myJson = JSON.stringify(obj)

	if (confirm("Data ingin disimpan ?") == true) {
		$.ajax({
			url: "http://localhost:81/api/tabalamat/update",
			type: "PUT",
			contentType: "application/json",
			data: myJson,
			success: function() {
				alert("Data diupdate")
				$("#modaltabalamat").modal('hide')
				kembali()
			}
		})
	} else {
		
	}
	window.close()

})
function kembali() {
	$.ajax({
		url: "http://localhost/viewtabalamat",
		type: "GET",
		datatype: "html",
		success: function(hasil) {
			$(".isimain").html(hasil)
		}
	})
}

//kembali ke hal tabalamat
$(".btnbatal").click(function() {
	$("#modaltabalamat").modal('hide')
})