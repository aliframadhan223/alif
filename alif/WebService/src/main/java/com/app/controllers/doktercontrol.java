package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class doktercontrol {

	@GetMapping("profiledokter")
	public String viewprofiledokter() {
		return "/profiledokter/profiledokter";
	}
	
	@GetMapping("tindakandokter")
	public String tindakandokter() {
		return "/profiledokter/tindakandokter";
	}
	
	@GetMapping("tambahtindakan")
	public String tambahtindakandokter() {
		return "/profiledokter/tambahtindakandokter";
	}
}