$(document).ready(function() {
	addlocation()
	function addlocation() {
        $.ajax({

            url: "http://localhost:81/api/tabalamat/listkotakec",
            type: "GET",
            success: function(hasil) {
                for (i = 0; i < hasil.length; i++) {
                    $(".ilocationid").append("<option value='" + hasil[i].id + "'>" + hasil[i].keckota + "</option>")
                }

            }
        })
    }
	
	for (i = 1; i <= 5; i++) {
		$(".eror" + i + "").hide()
	}

	valid1 = 0, valid2 = 0, valid3 = 0, valid4 = 0, valid5 = 0
	let biodataid = sessionStorage.getItem("biodataid")
	$(".btnsimpan").click(function() {
		if ($("#ilabel").val().length == 0 || $("#ilabel").val() == " ") {
			$(".eror1").show()
			valid1 += 1
		} else if ($("#ilabel").val().length != 0 || $("#ilabel").val() != " ") {
			$(".eror1").hide()
			valid1 = 0
		}

		if ($("#irecipient").val().length == 0 || $("#irecipient").val() == " ") {
			$(".eror2").show()
			valid2 += 1
		} else if ($("#irecipient").val().length != 0 || $("#irecipient").val() != " ") {
			$(".eror2").hide()
			valid2 = 0
		}

		if ($("#irecipientphonenumber").val().length == 0 || $("#irecipientphonenumber").val() == " ") {
			$(".eror3").show()
			valid3 += 1
		} else if ($("#irecipientphonenumber").val().length != 0 || $("#irecipientphonenumber").val() != " ") {
			$(".eror3").hide()
			valid3 = 0
		}

		if ($("#iaddress").val().length == 0 || $("#iaddress").val() == " ") {
			$(".eror5").show()
			valid5 += 1
		} else if ($("#iaddress").val().length != 0 || $("#iaddress").val() != " ") {
			$(".eror5").hide()
			valid5 = 0
		}

		if (valid1 == 0 && valid2 == 0 && valid3 == 0 && valid4 == 0 && valid5 == 0) {
			var obj = {}
			obj.biodataid = biodataid
			obj.label = $("#ilabel").val()
			obj.recipient = $("#irecipient").val()
			obj.recipientphonenumber = $("#irecipientphonenumber").val()
			obj.locationid = $("#ilocationid").val()
			obj.postalcode = $("#ipostalcode").val()
			obj.address = $("#iaddress").val()

			var myJson = JSON.stringify(obj)
			$.ajax({
				url: "http://localhost:81/api/tabalamat/add",
				type: "POST",
				contentType: "application/json",
				data: myJson,
				success: function() {
					$("#modaltabalamat").modal('hide') //show modal terlebih dahulu
					alert("simpan Berhasil")
					kembali()
				}
			})
		} else {
			return false
		}
	})

	function kembali() {
		$.ajax({
			url: "http://localhost/viewtabalamat",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$("#modaltabalamat").modal('hide')
				$(".isimain").html(hasil)
			}
		})
	}
	//kembali ke hal variants
	$(".btnbatal").click(function() {
		$("#modaltabalamat").modal('hide')
	})


})