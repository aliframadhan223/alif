$(document).ready(function() {
	tabalamatid()

	function tabalamatid() {
		let id = sessionStorage.getItem("id")
		$.ajax({
			url: "http://localhost:81/api/tabalamat/list/" + id,
			type: "GET",
			success: function(hasil) {
				$("#l").text(hasil.label)
			}
		})
	}

	$("#btnHapus").click(function() {
		let id = sessionStorage.getItem("id")

		var obj = {}
		obj.id = id

		var myjson = JSON.stringify(obj)

		$.ajax({
			url: "http://localhost:81/api/tabalamat/delete",
			type: "DELETE",
			contentType: "application/json",
			data: myjson,
			success: function(hasil) {
				$("#modaltabalamat").modal('hide')
				alert("Delete Berhasil")
				kembali()
			},
		})
	})

	function kembali() {
		$.ajax({
			url: "http://localhost/viewtabalamat",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$("#modaltabalamat").modal('hide')
				$(".isimain").html(hasil)
			}
		})
	}
	$("#btnBatal").click(function() {
		$("#modaltabalamat").modal('hide')
	})

})