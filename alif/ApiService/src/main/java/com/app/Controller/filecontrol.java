package com.app.Controller;

import java.io.File;
import java.io.IOException;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.app.ApiServiceApplication;

@RestController
@RequestMapping("api/file")
@CrossOrigin(origins = "*")
public class filecontrol {

	//get alamat upload dir
	public static String uploaddir= ApiServiceApplication.uploadDir;
	
	@PostMapping("upload")
	public void upload(@RequestParam("uploadFile")MultipartFile uploadfile) throws IllegalStateException, IOException {
		File file = new File(uploaddir+uploadfile.getOriginalFilename());
		uploadfile.transferTo(file);
	}
	
}
