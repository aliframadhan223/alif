package com.app.Controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.BiodataModels;
import com.app.Model.DoctorTreatmentModel;
import com.app.Model.TabAlamatModel;
import com.app.Repo.TabTindakanDokterRepo;

@RestController
@RequestMapping("api/tindakandokter")
@CrossOrigin(origins = "*")
public class TabTindakanDokterControl {

	@Autowired
	private TabTindakanDokterRepo ttdr;
	
	@GetMapping("list")
	public List<DoctorTreatmentModel> listtreatment(){
		return ttdr.listtindakan();
	}
	
	@PostMapping("add")
	public void addtindakan(@RequestBody DoctorTreatmentModel dtm) {
		LocalDateTime today = LocalDateTime.now();
		dtm.setCreatedby(dtm.getId());
		dtm.setCreatedon(today);
		ttdr.save(dtm);
	}
	
	@GetMapping("tindakandokterbyid/{id}")
	public List<Map<String, Object>> tindakandokterByid(@PathVariable long id) {
		return ttdr.tindakandokterbyid(id);
	}
	
	@DeleteMapping("deletetindakan")
	public void deletetindakan(@RequestBody DoctorTreatmentModel dtm) {
		LocalDateTime today = LocalDateTime.now();
		dtm.setDeletedby(dtm.getId());
		dtm.setDeletedon(today);
		ttdr.hapustindakan(dtm.getId());
	}
	
	@GetMapping("searchvalidasi/{id}/{tindakan}") 
	public List<DoctorTreatmentModel> searchTabAlamat(@PathVariable long id, @PathVariable String tindakan) {
		return ttdr.searchvalidasi(id, tindakan);
	}
}
