package com.app.Controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.BiodataModels;
import com.app.Repo.TabProfileRepo;

@RestController
@RequestMapping("api/tabprofile")
@CrossOrigin(origins = "*")
public class TabProfileControl {

	@Autowired
	private TabProfileRepo tpr;
	
	@GetMapping("listprofile/{id}")
	public List<Map<String, Object>> listprofile(@PathVariable long id){
		return tpr.listprofile(id);
	}
	
	@GetMapping("listprofiletahun/{id}")
	public BiodataModels listprofilethn(@PathVariable long id){
		return tpr.listprofiletahun(id);
	}
	
	@GetMapping("list2")
	public List<BiodataModels> listprofil2(){
		return tpr.listprofile2();
	}
}
