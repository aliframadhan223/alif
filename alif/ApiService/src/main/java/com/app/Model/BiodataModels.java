package com.app.Model;

import java.sql.Blob;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mBiodata")
public class BiodataModels {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "fullname", length = 255)
	private String fullname;
	
	@Column(name = "mobilePhone", length = 15)
	private String mobilephone;
	
	@Column(name = "image")
	private Blob image;
	
	@Column(name = "imagePath")
	private String imagepath;

	@Column(name = "createdBy")
	private long createdby;
	
	@Column(name = "createdOn")
	private LocalDateTime createdon;
	
	@Column(name = "modifiedBy")
	private long modifiedby;
	
	@Column(name = "modifiedOn")
	private LocalDateTime modifiedon;
	
	@Column(name = "deletedBy")
	private long deletedby;
	
	@Column(name = "deletedOn")
	private LocalDateTime deletedon;
	
	@Column(name = "isDelete", columnDefinition = "boolean default false")
	private boolean delete;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getMobilephone() {
		return mobilephone;
	}

	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}

	public Blob getImage() {
		return image;
	}

	public void setImage(Blob image) {
		this.image = image;
	}

	public String getImagepath() {
		return imagepath;
	}

	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}

	public long getCreatedby() {
		return createdby;
	}

	public void setCreatedby(long createdby) {
		this.createdby = createdby;
	}

	public LocalDateTime getCreatedon() {
		return createdon;
	}

	public void setCreatedon(LocalDateTime createdon) {
		this.createdon = createdon;
	}

	public long getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(long modifiedby) {
		this.modifiedby = modifiedby;
	}

	public LocalDateTime getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(LocalDateTime modifiedon) {
		this.modifiedon = modifiedon;
	}

	public long getDeletedby() {
		return deletedby;
	}

	public void setDeletedby(long deletedby) {
		this.deletedby = deletedby;
	}

	public LocalDateTime getDeletedon() {
		return deletedon;
	}

	public void setDeletedon(LocalDateTime deletedon) {
		this.deletedon = deletedon;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	
}
