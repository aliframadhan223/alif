package com.app.Model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mUser")
public class UserModels {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name="email",nullable = false, length=100 )
	private String email;
	
	@Column(name="password",nullable = false, length=255 )
	private String password;
	
	@Column(name="biodata_id")
	private long biodataid;
	
	@Column(name="role_id")
	private long roleid;
	
	@Column(name="login_attempt")
	private int loginattempt;

	@Column(name="is_locked")
	private boolean islocked;

	@Column(name="last_login")
	private LocalDateTime lastlogin;
	
	@Column(name = "createdBy", columnDefinition = "")
	private long created_by;
	
	@Column(name = "createdOn",columnDefinition = "")
	private LocalDateTime created_on;
	
	@Column(name = "modifiedBy", columnDefinition = "bigint default 0")
	private long modified_by;
	
	@Column(name = "modifiedOn", columnDefinition = "timestamp default NOW()")
	private LocalDateTime modified_on;
	
	@Column(name = "deletedBy",columnDefinition = "bigint default 0")
	private long deleteded_by;
	
	@Column(name = "deletedOn",columnDefinition = "timestamp default NOW()")
	private LocalDateTime deleted_on;
	
	@Column(name = "isDelete", columnDefinition = "boolean default false")
	private boolean delete;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getBiodataid() {
		return biodataid;
	}

	public void setBiodataid(long biodataid) {
		this.biodataid = biodataid;
	}

	public long getRoleid() {
		return roleid;
	}

	public void setRoleid(long roleid) {
		this.roleid = roleid;
	}

	public int getLoginattempt() {
		return loginattempt;
	}

	public void setLoginattempt(int loginattempt) {
		this.loginattempt = loginattempt;
	}

	public boolean isIslocked() {
		return islocked;
	}

	public void setIslocked(boolean islocked) {
		this.islocked = islocked;
	}

	public LocalDateTime getLastlogin() {
		return lastlogin;
	}

	public void setLastlogin(LocalDateTime lastlogin) {
		this.lastlogin = lastlogin;
	}

	public long getCreated_by() {
		return created_by;
	}

	public void setCreated_by(long created_by) {
		this.created_by = created_by;
	}

	public LocalDateTime getCreated_on() {
		return created_on;
	}

	public void setCreated_on(LocalDateTime created_on) {
		this.created_on = created_on;
	}

	public long getModified_by() {
		return modified_by;
	}

	public void setModified_by(long modified_by) {
		this.modified_by = modified_by;
	}

	public LocalDateTime getModified_on() {
		return modified_on;
	}

	public void setModified_on(LocalDateTime modified_on) {
		this.modified_on = modified_on;
	}

	public long getDeleteded_by() {
		return deleteded_by;
	}

	public void setDeleteded_by(long deleteded_by) {
		this.deleteded_by = deleteded_by;
	}

	public LocalDateTime getDeleted_on() {
		return deleted_on;
	}

	public void setDeleted_on(LocalDateTime deleted_on) {
		this.deleted_on = deleted_on;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}
}
