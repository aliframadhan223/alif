package com.app.Model;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mCustomer")
public class CustomerModels {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "biodata_id")
	private  long biodata_id;
	
	@Column(name = "dob")
	private Date dob;
	
	@Column(name = "gender")
	private String gender;
	
	@Column(name = "blood_group_id")
	private  long blood_group_id;
	
	@Column(name = "rhesus_type")
	private  String rhesus_type;
	
	@Column(name = "height")
	private double height;
	
	@Column(name = "weight")
	private double weight;
	
	@Column(name = "created_by")
	private long createdby;

	@Column(name = "created_on")
	private LocalDateTime createdon;
	
	@Column(name = "modifed_by")
	private long modifedby;
	
	@Column(name = "modifed_on")
	private LocalDateTime modifedon;
	
	@Column(name = "deleted_by")
	private long deletedby;
	
	@Column(name = "deleted_on")
	private LocalDateTime deletedon;
	
	@Column(name = "is_delete", columnDefinition = "boolean default false")
	private boolean delete ;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getBiodata_id() {
		return biodata_id;
	}

	public void setBiodata_id(long biodata_id) {
		this.biodata_id = biodata_id;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public long getBlood_group_id() {
		return blood_group_id;
	}

	public void setBlood_group_id(long blood_group_id) {
		this.blood_group_id = blood_group_id;
	}

	public String getRhesus_type() {
		return rhesus_type;
	}

	public void setRhesus_type(String rhesus_type) {
		this.rhesus_type = rhesus_type;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public long getCreatedby() {
		return createdby;
	}

	public void setCreatedby(long createdby) {
		this.createdby = createdby;
	}

	public LocalDateTime getCreatedon() {
		return createdon;
	}

	public void setCreatedon(LocalDateTime createdon) {
		this.createdon = createdon;
	}

	public long getModifedby() {
		return modifedby;
	}

	public void setModifedby(long modifedby) {
		this.modifedby = modifedby;
	}

	public LocalDateTime getModifedon() {
		return modifedon;
	}

	public void setModifedon(LocalDateTime modifedon) {
		this.modifedon = modifedon;
	}

	public long getDeletedby() {
		return deletedby;
	}

	public void setDeletedby(long deletedby) {
		this.deletedby = deletedby;
	}

	public LocalDateTime getDeletedon() {
		return deletedon;
	}

	public void setDeletedon(LocalDateTime deletedon) {
		this.deletedon = deletedon;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	
	
}
