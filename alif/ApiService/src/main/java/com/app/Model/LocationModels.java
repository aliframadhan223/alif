package com.app.Model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mLocation")
public class LocationModels {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "parentId")
	private long parentid;
	
	@Column(name = "locationLevelId")
	private long locationlevelid;
	
	@Column(name = "createdBy", columnDefinition = "")
	private long created_by;
	
	@Column(name = "createdOn",columnDefinition = "")
	private LocalDateTime created_on;
	
	@Column(name = "modifiedBy", columnDefinition = "bigint default 0")
	private long modified_by;
	
	@Column(name = "modifiedOn", columnDefinition = "timestamp default NOW()")
	private LocalDateTime modified_on;
	
	@Column(name = "deletedBy",columnDefinition = "bigint default 0")
	private long deleteded_by;
	
	@Column(name = "deletedOn",columnDefinition = "timestamp default NOW()")
	private LocalDateTime deleted_on;
	
	@Column(name = "isDelete", columnDefinition = "boolean default false")
	private boolean delete;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public long getParentid() {
		return parentid;
	}

	public void setParentid(long parentid) {
		this.parentid = parentid;
	}

	public long getLocationlevelid() {
		return locationlevelid;
	}

	public void setLocationlevelid(long locationlevelid) {
		this.locationlevelid = locationlevelid;
	}

	public long getCreated_by() {
		return created_by;
	}

	public void setCreated_by(long created_by) {
		this.created_by = created_by;
	}

	public LocalDateTime getCreated_on() {
		return created_on;
	}

	public void setCreated_on(LocalDateTime created_on) {
		this.created_on = created_on;
	}

	public long getModified_by() {
		return modified_by;
	}

	public void setModified_by(long modified_by) {
		this.modified_by = modified_by;
	}

	public LocalDateTime getModified_on() {
		return modified_on;
	}

	public void setModified_on(LocalDateTime modified_on) {
		this.modified_on = modified_on;
	}

	public long getDeleteded_by() {
		return deleteded_by;
	}

	public void setDeleteded_by(long deleteded_by) {
		this.deleteded_by = deleteded_by;
	}

	public LocalDateTime getDeleted_on() {
		return deleted_on;
	}

	public void setDeleted_on(LocalDateTime deleted_on) {
		this.deleted_on = deleted_on;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	
}
