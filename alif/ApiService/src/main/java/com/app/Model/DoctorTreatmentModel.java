package com.app.Model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tDoctorTreatment")
public class DoctorTreatmentModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "doctorId")
	private long doctorid;
	
	@Column(name = "name",length = 50)
	private String name;
	
	@Column(name = "createdBy")
	private long createdby;
	
	@Column(name = "createdOn")
	private LocalDateTime createdon;
	
	@Column(name = "modifiedBy")
	private long modifiedby;
	
	@Column(name = "modifiedOn")
	private LocalDateTime modifiedon;
	
	@Column(name = "deletedBy")
	private long deletedby;
	
	@Column(name = "deletedOn")
	private LocalDateTime deletedon;
	
	@Column(name = "isDelete", columnDefinition = "boolean default false")
	private boolean delete;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getDoctorid() {
		return doctorid;
	}

	public void setDoctorid(long doctorid) {
		this.doctorid = doctorid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getCreatedby() {
		return createdby;
	}

	public void setCreatedby(long createdby) {
		this.createdby = createdby;
	}

	public LocalDateTime getCreatedon() {
		return createdon;
	}

	public void setCreatedon(LocalDateTime createdon) {
		this.createdon = createdon;
	}

	public long getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(long modifiedby) {
		this.modifiedby = modifiedby;
	}

	public LocalDateTime getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(LocalDateTime modifiedon) {
		this.modifiedon = modifiedon;
	}

	public long getDeletedby() {
		return deletedby;
	}

	public void setDeletedby(long deletedby) {
		this.deletedby = deletedby;
	}

	public LocalDateTime getDeletedon() {
		return deletedon;
	}

	public void setDeletedon(LocalDateTime deletedon) {
		this.deletedon = deletedon;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}
}
