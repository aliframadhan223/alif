package com.app.Model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mLocationLevel")
public class LocationLvlModels {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id ;
	
	@Column(name = "name", length = 50)
	private String name;
	
	@Column(name = "abbreviation", length = 50)
	private String abbreviation;
	
	@Column(name = "createdBy", columnDefinition = "")
	private long createdby;
	
	@Column(name = "createdOn",columnDefinition = "")
	private LocalDateTime createdon;
	
	@Column(name = "modifiedBy", columnDefinition = "bigint default 0")
	private long modifiedby;
	
	@Column(name = "modifiedOn", columnDefinition = "timestamp default NOW()")
	private LocalDateTime modifiedon;
	
	@Column(name = "deletedBy",columnDefinition = "bigint default 0")
	private long deletededby;
	
	@Column(name = "deletedOn",columnDefinition = "timestamp default NOW()")
	private LocalDateTime deletedon;
	
	@Column(name = "isDelete", columnDefinition = "boolean default false")
	private boolean delete;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public long getCreatedby() {
		return createdby;
	}

	public void setCreatedby(long createdby) {
		this.createdby = createdby;
	}

	public LocalDateTime getCreatedon() {
		return createdon;
	}

	public void setCreatedon(LocalDateTime createdon) {
		this.createdon = createdon;
	}

	public long getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(long modifiedby) {
		this.modifiedby = modifiedby;
	}

	public LocalDateTime getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(LocalDateTime modifiedon) {
		this.modifiedon = modifiedon;
	}

	public long getDeletededby() {
		return deletededby;
	}

	public void setDeletededby(long deletededby) {
		this.deletededby = deletededby;
	}

	public LocalDateTime getDeletedon() {
		return deletedon;
	}

	public void setDeletedon(LocalDateTime deletedon) {
		this.deletedon = deletedon;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}
}
