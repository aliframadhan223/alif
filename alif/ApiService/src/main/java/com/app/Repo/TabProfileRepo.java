package com.app.Repo;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.BiodataModels;

public interface TabProfileRepo extends JpaRepository<BiodataModels, Long> {

	@Query(value = "select mb.id as id, mb.fullname, mb.mobile_phone as mphone,mb.created_on as createdon, mu.email, mu.password, to_char(mc.dob,'dd/mm/yyyy') as dob "
			+ " from m_biodata mb join m_user mu "
			+ " on mb.id = mu.biodata_id"
			+ " join m_customer mc "
			+ " on mb.id = mc.biodata_id where mb.id=:id", nativeQuery = true)
	List<Map<String, Object>> listprofile(long id);
	
	@Query(value = "select * from m_biodata where id = :id limit 1",nativeQuery = true)
	BiodataModels listprofiletahun (long id);
	
	@Query(value = "select * from m_biodata order by id", nativeQuery = true)
	List<BiodataModels> listprofile2();
}
