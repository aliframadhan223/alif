package com.app;

import java.io.File;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiServiceApplication {
	//acuan folder
		public static String uploadDir = System.getProperty("user.dir")+"/uploadDir/";
		public static void main(String[] args) {
			SpringApplication.run(ApiServiceApplication.class, args);
			
			//buat direktori ketika running
			new File(uploadDir).mkdir();
		}
}
