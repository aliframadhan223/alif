insert into m_biodata_address 
(biodata_id,label,recipient,recipient_phone_number,location_id,postal_code,address,created_by,created_on,modified_by,deleted_by)
values 
(1,'Rumah1','Juliantoroo','081284478711',2,'46578','Jl.Anyer Panarukan',2,now(),0,0),
(1,'Rumah2','Juliantorow','081284478712',3,'46578','Jl.Anyer Panarukan',2,now(),0,0),
(2,'Rumah3','Juliantoroq','081284478713',4,'46578','Jl.Anyer Panarukan',2,now(),0,0),
(2,'Rumah4','Juliantoroe','081284478714',5,'46578','Jl.Anyer Panarukan',2,now(),0,0),
(3,'Kantor','Wati','087889085764',1,'22355','Jl.Jalan dulu',1,now(),0,0);

insert into m_user
(biodata_id,role_id,email,password)
values
(1,1,'jamal@gmail.com','12345'),
(2,2,'fahrul22@gmail.com','54321');

insert into m_customer
(biodata_id,dob) values  
(1,'1999-01-22'),
(2,'1998-01-21');

insert into m_biodata
(fullname,mobile_phone,created_on,created_by,modified_by,deleted_by) values
('Dr.Richard Lee',08937475324,now(),0,0,0),
('Dr.Johan Silalahi',0888394354,now(),0,0,0);

insert into m_location 
(name,location_level_id)
values 
('Kebayoran Lama',1), 
('Tanah Abang',1),
('Jakarta Selatan',2);

insert into m_location_level (name,abbreviation) values ('Kecamata','Kec.'), ('Kota ','Kota ');

insert into t_doctor_treatment (doctor_id,name,created_on,created_by,modified_by,deleted_by) values 
(1,'Fisioterapi Anak',now(),0,0,0),(2,'Kesehatan Anak',now(),0,0,0);

insert into m_doctor (biodata_id,str) values (1,'Aktif'),(2,'Aktif');