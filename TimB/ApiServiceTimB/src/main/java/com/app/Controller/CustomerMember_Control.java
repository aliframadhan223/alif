package com.app.Controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.CustomerMemberModel;
import com.app.Repo.CustomerMemberRepo;

@RestController
@RequestMapping("api/customer_member")
@CrossOrigin(origins = "*")
public class CustomerMember_Control {
	
	@Autowired
	private CustomerMemberRepo cmr;
	
	@PostMapping("post")
	public void postmember(@RequestBody CustomerMemberModel cmm) {
		cmm.setCreatedOn(LocalDateTime.now());
		cmm.setCreatedBy(1);
		cmr.save(cmm);
	}
}
