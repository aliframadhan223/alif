package com.app.Controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.CustomerMemberModel;
import com.app.Model.DoctorModel;
import com.app.Repo.DoctorRepo;

@RestController
@RequestMapping("api/doctor")
@CrossOrigin(origins = "*")
public class DoctorControl {

	@Autowired
	private DoctorRepo dr;

//	@GetMapping("list")
//	public List<Map<Long, Object>> listDoctor() {
//		return dr.listDoctor();
//	}

	@PostMapping("post")
	public void post(@RequestBody DoctorModel dm) {
		dm.setC_on(LocalDateTime.now());
		dm.setC_by((long) 1);
		dr.save(dm);
	}

	@GetMapping("poplocs")
	public List<Map<Long, Object>> popLocs() {
		return dr.popLocs();
	}

	@GetMapping("popname")
	public List<Map<Long, Object>> popName() {
		return dr.popName();
	}

	@GetMapping("popspec")
	public List<Map<Long, Object>> popSpec() {
		return dr.popSpec();
	}

	@GetMapping("popmedt")
	public List<Map<Long, Object>> popMedt() {
		return dr.popMedt();
	}

	@GetMapping("popmedt/{spec}")
	public List<Map<Long, Object>> popMedtBySpec(@PathVariable Long spec) {
		return dr.popMedtBySpec(spec);
	}

	@GetMapping("caridoctor")
	public List<Map<Long, Object>> cariDoctor(@RequestParam Long specId, @RequestParam String name,
			@RequestParam Long medtId, @RequestParam Long locId) {
		return dr.cariDoctor(specId, name, medtId, locId);
	}

	@GetMapping("listlocation")
	public List<Map<Long, Object>> listLocation(@RequestParam Long idDoctor) {
		return dr.ListLocation(idDoctor);
	}

	@GetMapping("listdatadoctor")
	public List<Map<Long, Object>> listDataDoctor(@RequestParam Long idDoctor) {
		return dr.ListDataDoctor(idDoctor);
	}

	@GetMapping("headdoctordetail")
	public List<Map<Long, Object>> HeadDetailDoctor(@RequestParam Long idDoctor) {
		return dr.HeadDetailDoctor(idDoctor);
	}

	@GetMapping("tindakanmedisdetail")
	public List<Map<Long, Object>> TindakanMedisByIdDoctor(@RequestParam Long idDoctor) {
		return dr.TindakanMedisByIdDoctor(idDoctor);
	}

	@GetMapping("riwayatpraktekdetail")
	public List<Map<Long, Object>> RiwayatPraktekByIdDoctor(@RequestParam Long idDoctor) {
		return dr.RiwayatPraktekByIdDoctor(idDoctor);
	}

	@GetMapping("pendidikandokterdetail")
	public List<Map<Long, Object>> PendidikanDokterByIdDoctor(@RequestParam Long idDoctor) {
		return dr.PendidikanDokterByIdDoctor(idDoctor);
	}

	@GetMapping("jadwaldokterdetail")
	public List<Map<Long, Object>> JadwalPraktekByIdDoctor(@RequestParam Long idDoctor) {
		return dr.JadwalPraktekByIdDoctor(idDoctor);
	}

	@GetMapping("lokasipraktekdetail")
	public List<Map<Long, Object>> LokasiPraktekIdDoctor(@RequestParam Long idDoctor) {
		return dr.LokasiPraktekIdDoctor(idDoctor);
	}
}
