package com.app.Controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.BloodGroupModel;
import com.app.Repo.BloodGroupRepositorys;


@RestController
@RequestMapping("api/blood")
@CrossOrigin(origins = "*")
public class BloodGroupControl {
	
	@Autowired
	private BloodGroupRepositorys bgr;
	
	@PostMapping("addblood")
	public void addBlood(@RequestBody BloodGroupModel bgm) {
		LocalDateTime today = LocalDateTime.now();
		bgm.setCreatedBy(1);
		bgm.setCreatedOn(today);
		bgr.save(bgm);
		
	}
	
	@GetMapping("list")
	public List<BloodGroupModel> listbloodactive(){
		return bgr.listbloodActive();
	}
	
	@PutMapping("edit")
	public void editBlood(@RequestBody BloodGroupModel bgm) {
		LocalDateTime today = LocalDateTime.now();
		bgm.setModifedBy(1);
		bgm.setModifedOn(today);
		bgr.save(bgm);
	}
	
	@DeleteMapping("delete")
	public void deleteblood(@RequestBody BloodGroupModel bgm) {
		LocalDateTime today = LocalDateTime.now();
		bgm.setDeletedBy(1);
		bgm.setDeletedOn(today);
		bgr.deleteblood(bgm.getId());
	}
	
	@GetMapping("listbyname/{name}")
	public List<BloodGroupModel> listbyname(@PathVariable String name){
		return bgr.listBloodbyname(name);
	}
	@GetMapping("list/{id}")
	public BloodGroupModel bloodsById(@PathVariable long id) {
		return bgr.BloodById(id);
	}
}
