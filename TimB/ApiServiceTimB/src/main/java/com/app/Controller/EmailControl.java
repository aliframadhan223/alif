package com.app.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.pesanModel;
import com.app.com.MailConfig;


@RestController
@RequestMapping("api/email")
@CrossOrigin("*")
public class EmailControl {

	@Autowired
	private MailConfig mc;
	
	@PostMapping("kirim")
	public void kirim(@RequestBody pesanModel pm) {
		mc.kirim(pm.getTo(), pm.getSubject(), pm.getBody());
	}
}
