package com.app.Controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.AdminModel;
import com.app.Model.tokenModel;
import com.app.Repo.AdminRepo;


@RestController
@RequestMapping("api/admin")
@CrossOrigin(origins = "*")
public class AdminRestControl {
	@Autowired
	private AdminRepo ar;
	
	@GetMapping("list")
	public List<AdminModel> listProductActive(){
		return ar.listadmin();
	}
	@PostMapping("add")
	public void addProduct( @RequestBody AdminModel am) {
		
		LocalDateTime today = LocalDateTime.now();
		am.setCreateon(today);
		am.setCreateby(1);
		ar.save(am);
	}
	
	@GetMapping("list/{id}")
	public AdminModel adminById(@PathVariable long id){
		return ar.adminbyid(id);
	}
	
	@PutMapping("update")
	public void updateadmin(@RequestBody AdminModel am) {
		LocalDateTime today = LocalDateTime.now();
		am.setModifiedon(today);
		am.setModifiedby(1);
		ar.save(am);
	}
	
	@DeleteMapping("delete")
	public void deleteadmin(@RequestBody AdminModel am){
		LocalDateTime today = LocalDateTime.now();
		am.setDeleteon(today);
		am.setModifiedby(1);
		ar.deleteadmin(am.getId());
	}
	
	@PostMapping("post")
	public void postToken(@RequestBody AdminModel am) {
		am.setCreateby(0);
		am.setCreateon(LocalDateTime.now());
		ar.save(am);

		
	}

}
