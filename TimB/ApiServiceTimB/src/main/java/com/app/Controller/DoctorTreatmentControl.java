package com.app.Controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.DoctorTreatmentModel;
import com.app.Repo.DoctorTreatmentRepo;

@RestController
@RequestMapping("api/tindakandokter")
@CrossOrigin(origins = "*")
public class DoctorTreatmentControl {

	@Autowired
	private DoctorTreatmentRepo dtr;
	
	@PostMapping("add")
	public void addtindakan(@RequestBody DoctorTreatmentModel dtm) {
		LocalDateTime today = LocalDateTime.now();
		dtm.setC_by(dtm.getId());
		dtm.setC_on(today);
		dtm.setIs_del(false);
		dtr.save(dtm);
	}
	
	@GetMapping("tindakandokterbyid/{id}")
	public List<Map<String, Object>> tindakandokterByid(@PathVariable long id) {
		return dtr.tindakandokterbyid(id);
	}
	
	@GetMapping("tindakanid/{id}")
	public DoctorTreatmentModel tindakan(@PathVariable long id) {
		return dtr.listtindakan(id);
	}
	
	@DeleteMapping("deletetindakan")
	public void deletetindakan(@RequestBody DoctorTreatmentModel dtm) {
		LocalDateTime today = LocalDateTime.now();
		dtm.setD_by(dtm.getId());
		dtm.setD_on(today);
		dtm.setIs_del(true);
		dtr.hapustindakan(dtm.getId());
	}
	
	@GetMapping("searchvalidasi/{id}/{tindakan}") 
	public List<DoctorTreatmentModel> searchDoctortreatment(@PathVariable long id, @PathVariable String tindakan) {
		return dtr.searchvalidasi(id, tindakan);
	}
}
