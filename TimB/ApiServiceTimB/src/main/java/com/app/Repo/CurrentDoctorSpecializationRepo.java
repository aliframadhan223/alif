package com.app.Repo;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.CurrentDoctorSpecializationModel;

public interface CurrentDoctorSpecializationRepo extends JpaRepository<CurrentDoctorSpecializationModel, Long>{
	//@Query(value = " select cds.id,cds.doctor_id,cds.specialization_id from m_specialization s join t_current_doctor_specialization "
			//+ " cds on cds.specialization_id=s.id join m_doctor d on d.id=cds.doctor_id join m_biodata mb "
			//+ " on d.biodata_id=mb.id where d.is_delete=false and mb.id= :id ")
}
