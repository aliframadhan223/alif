package com.app.Repo;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.roleModel;


@Transactional
public interface roleRepo extends JpaRepository<roleModel, Long> {

	@Query(value="select id, name, code from m_role where is_delete=false", nativeQuery = true)
	List<Map<String, Object>>roleorder();
	

	@Query(value = "select * from m_role where id= :id limit 1", nativeQuery = true)
	roleModel rolebyid(long id);
	
	@Modifying
	@Query(value = "update m_role set is_delete=true where id= :id ", nativeQuery = true)
	int deleterole(long id);
}
