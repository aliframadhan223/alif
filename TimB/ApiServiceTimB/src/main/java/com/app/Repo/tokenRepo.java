package com.app.Repo;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.tokenModel;


@Transactional
public interface tokenRepo extends JpaRepository<tokenModel, Long> {

	//cek list token
	@Query(value="select * from token",nativeQuery = true)
	List<tokenModel>tokeorder();
	
	//tampilan list token yang dibutuhkan
	@Query(value="select id, email, user_id,token,expired,"
			+ "is_expired,used_for from t_token where is_delete=false",nativeQuery = true)
	List<Map<String, Object>>listToken();
	
	//mengurutkan email baru dibuat	
	@Query(value = "select id from t_token where email= :email order by id desc limit 1", nativeQuery = true)
	String selectToken(String email);
	
	//ambil token berdasarkan id
	@Query(value = "select token from t_token where email= :email and is_expired=false limit 1", nativeQuery = true)
	String sendToken(String email);
	
	//mengecek valid token
	@Query(value = "select coalesce(sum(id),0) from t_token where id = :id and"
			+ " token= :token", nativeQuery = true)
	int cek_Token(int id, String token);
	
	//cek expired token
	@Query(value = "select coalesce(count(is_expired),0) from t_token where id = :id and is_expired= false " ,nativeQuery = true)
	String cekValid(int id);
	
	//update filed expired setiap detik
	@Modifying
	@Query(value = "update t_token set is_expired=true where expired < now()", nativeQuery = true)
	void updateExpired();
		
		//expiring token by id
	@Modifying
	@Query(value = "update t_token set is_expired=true where id= :id", nativeQuery = true)
	void setExpired(int id);
	
	
	
	
	
}
