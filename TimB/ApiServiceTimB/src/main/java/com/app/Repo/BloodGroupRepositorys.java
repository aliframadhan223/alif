package com.app.Repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.BloodGroupModel;


@Transactional
public interface BloodGroupRepositorys  extends JpaRepository<BloodGroupModel, Long>{
	
	@Query(value="select * from m_bloodgroup where is_delete = false order by id",nativeQuery = true)
	List<BloodGroupModel> listbloodActive();
	
	@Modifying
	@Query(value = "update m_bloodgroup set is_delete = true where id=:id",nativeQuery = true)
	int deleteblood(long id);
	
	@Query(value = "select * from m_bloodgroup where is_delete= false and lower(code) like lower(concat('%',:nm,'%'))",nativeQuery = true)
	List<BloodGroupModel> listBloodbyname(String nm);
	
	@Query(value="select * from m_bloodgroup where id=:id limit 1",nativeQuery = true)
	BloodGroupModel BloodById(long id);
}
