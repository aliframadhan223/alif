package com.app.Repo;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.Model.SpecializationModel;

@Transactional
public interface SpecializationRepo extends JpaRepository<SpecializationModel, Long>{

}
