package com.app.Repo;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.Model.DoctorModel;

@Transactional
public interface DoctorOfficeRepo extends JpaRepository<DoctorModel, Long>{

}
