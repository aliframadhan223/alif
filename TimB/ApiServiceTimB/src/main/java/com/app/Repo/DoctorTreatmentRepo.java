package com.app.Repo;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.DoctorTreatmentModel;

@Transactional
public interface DoctorTreatmentRepo extends JpaRepository<DoctorTreatmentModel, Long> {

	@Query(value = "select * from t_doctor_treatment where is_delete=false and id=:id limit 1", nativeQuery = true)
	DoctorTreatmentModel listtindakan(long id);
	
	@Query(value = " select b.fullname as namadokter, tdt.name as name, tdt.id as id, "
			+ " tdt.doctor_id as doctorid "
			+ " from (select * from t_doctor_treatment where is_delete=false) as tdt "
			+ " join m_doctor d on d.id=tdt.doctor_id "
			+ " join m_biodata b on b.id = d.biodata_id where d.biodata_id= :id",nativeQuery = true)
	List<Map<String, Object>> tindakandokterbyid(long id);
	
	@Modifying
	@Query(value = "update t_doctor_treatment set is_delete=true where id=:id",nativeQuery = true)
	int hapustindakan(long id);
	
	@Query(value = "select * from (select * from t_doctor_treatment where is_delete=false) as tdt where doctor_id=:id and lower(name) like :tindakan",nativeQuery = true)
	List<DoctorTreatmentModel> searchvalidasi(long id,String tindakan);

}
