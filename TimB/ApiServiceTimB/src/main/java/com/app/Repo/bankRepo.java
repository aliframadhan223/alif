package com.app.Repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.bankModel;




@Transactional
public interface bankRepo extends JpaRepository<bankModel, Long> {
	
	@Query(value="select * from m_bank where is_delete = false order by id",nativeQuery = true)
	List<bankModel>bankorder();
	
	//mecari bak berdasarkan id
	@Query(value = "select * from m_bank where id= :id limit 1",nativeQuery = true)
	bankModel bankById(long id);
	
	//cari nama
	@Query(value = "select * from m_bank where lower(name) like lower(concat('%',:nm,'%')) and is_delete = 'false' order by id",nativeQuery = true) //harus di concat kalo ingin huruf kecil
	List<bankModel> bankByNama(String nm);
	
	@Query(value = "select * from m_bank where lower(va_code) like lower(concat('%',:va,'%')) or lower(name) like lower(concat('%', :nm ,'%')) and is_delete = 'false' order by id",nativeQuery = true)
	List<bankModel> bankByNamaKode(String nm,String va);
	
	@Query(value = "select * from m_bank where lower(va_code) like lower(concat('%',:va,'%')) and is_delete = 'false' order by id",nativeQuery = true) //harus di concat kalo ingin huruf kecil
	List<bankModel> bankByKode(String va);
	
	
	@Modifying
	@Query(value = "update m_bank set is_delete = true where id= :id  ",nativeQuery = true)
	int deleteBank(long id);
	
	@Query(value = "select max(id) from m_bank",nativeQuery = true)
	Long getlastid();
	
	@Query(value = "select count(id) from m_bank where name= :nm and is_delete = false",nativeQuery = true)
	Long getCountid(String nm);
	
}


