package com.app.Repo;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.TabAlamatModel;

@Transactional
public interface TabAlamatRepo extends JpaRepository<TabAlamatModel, Long> {

	@Query(value = "select * from m_biodata_address where is_delete = false and biodata_id=:biodataid order by label", nativeQuery = true)
	List<TabAlamatModel> listTabAlamatActive(long biodataid);

	@Query(value = "select * from (select * from m_biodata_address where is_delete = false and biodata_id=:biodataid) as bapuk where lower(label) like lower(concat('%',:a,'%')) or lower(recipient) like lower(concat('%',:a,'%'))  order by id", nativeQuery = true)
	List<TabAlamatModel> searchTabAlamat(String a, long biodataid);
	
	@Query(value = "select l.id as id, CONCAT(LL.abbreviation,' ',l.name) keckota from m_location l JOIN m_location_level ll ON ll.id = l.location_level_id",nativeQuery = true)
	List<Map<String, Object>> listlokasi();
	
	@Query(value = "select * from (select * from m_biodata_address where is_delete=false) as mba \r\n"
			+ "where biodata_id= :id and lower(label) like :label", nativeQuery = true)
	List<TabAlamatModel> ValidasiDuplikat(long id, String label);
	
	@Query(value = "select * from m_biodata_address where id = :id limit 1",nativeQuery = true)
	TabAlamatModel tabalamatbyid(long id);
	
	@Modifying
	@Query(value = "update m_biodata_address set is_delete = true where id =:id",nativeQuery = true)
	int deleteTabAlamat(long id);
	
}
