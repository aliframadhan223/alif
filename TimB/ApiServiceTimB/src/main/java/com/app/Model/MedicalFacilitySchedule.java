package com.app.Model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.lang.Nullable;

@Entity
@Table(name = "m_medical_facility_schedule")
public class MedicalFacilitySchedule {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Nullable
	@Column(name = "medical_facility_id")
	private Long medicalFacilityId;

	@Nullable
	@Size(max=10)
	@Column(name = "day")
	private String day;

	@Nullable
	@Size(max=10)
	@Column(name = "time_schedule_start")
	private String timeScheduleStart;

	@Nullable
	@Size(max=10)
	@Column(name = "time_schedule_end")
	private String timeScheduleEnd;
	
	@NotNull
	@Column(name = "created_by")
	private Long c_by;

	@NotNull
	@Column(name = "created_on")
	private LocalDateTime c_on;
	
	@Nullable
	@Column(name = "modified_by")
	private Long m_by;

	@Nullable
	@Column(name = "modified_on")
	private LocalDateTime m_on;
	
	@Nullable
	@Column(name = "deleted_by")
	private Long d_by;

	@Nullable
	@Column(name = "deleted_on")
	private LocalDateTime d_on;

	@Column(name = "is_delete", columnDefinition = "boolean default false")
	private Boolean is_del;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMedicalFacilityId() {
		return medicalFacilityId;
	}

	public void setMedicalFacilityId(Long medicalFacilityId) {
		this.medicalFacilityId = medicalFacilityId;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getTimeScheduleStart() {
		return timeScheduleStart;
	}

	public void setTimeScheduleStart(String timeScheduleStart) {
		this.timeScheduleStart = timeScheduleStart;
	}

	public String getTimeScheduleEnd() {
		return timeScheduleEnd;
	}

	public void setTimeScheduleEnd(String timeScheduleEnd) {
		this.timeScheduleEnd = timeScheduleEnd;
	}

	public Long getC_by() {
		return c_by;
	}

	public void setC_by(Long c_by) {
		this.c_by = c_by;
	}

	public LocalDateTime getC_on() {
		return c_on;
	}

	public void setC_on(LocalDateTime c_on) {
		this.c_on = c_on;
	}

	public Long getM_by() {
		return m_by;
	}

	public void setM_by(Long m_by) {
		this.m_by = m_by;
	}

	public LocalDateTime getM_on() {
		return m_on;
	}

	public void setM_on(LocalDateTime m_on) {
		this.m_on = m_on;
	}

	public Long getD_by() {
		return d_by;
	}

	public void setD_by(Long d_by) {
		this.d_by = d_by;
	}

	public LocalDateTime getD_on() {
		return d_on;
	}

	public void setD_on(LocalDateTime d_on) {
		this.d_on = d_on;
	}

	public Boolean getIs_del() {
		return is_del;
	}

	public void setIs_del(Boolean is_del) {
		this.is_del = is_del;
	}

}
