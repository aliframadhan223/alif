package com.app.Model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

@Entity
@Table(name = "t_doctor_office_schedule")
public class DoctorOfficeScheduleModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "doctor_id")
	private Long doctorId;

	@Column(name = "medical_facility_schedule_id")
	private Long medicalFacilityScheduleId;
	
	@Column(name = "slot")
	private Integer slot;
	
	@NotNull
	@Column(name = "created_by")
	private Long c_by;

	@NotNull
	@Column(name = "created_on")
	private LocalDateTime c_on;
	
	@Nullable
	@Column(name = "modified_by")
	private Long m_by;

	@Nullable
	@Column(name = "modified_on")
	private LocalDateTime m_on;
	
	@Nullable
	@Column(name = "deleted_by")
	private Long d_by;

	@Nullable
	@Column(name = "deleted_on")
	private LocalDateTime d_on;

	@Column(name = "is_delete", columnDefinition = "boolean default false")
	private Boolean is_del;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public Long getMedicalFacilityScheduleId() {
		return medicalFacilityScheduleId;
	}

	public void setMedicalFacilityScheduleId(Long medicalFacilityScheduleId) {
		this.medicalFacilityScheduleId = medicalFacilityScheduleId;
	}

	public Integer getSlot() {
		return slot;
	}

	public void setSlot(Integer slot) {
		this.slot = slot;
	}

	public Long getC_by() {
		return c_by;
	}

	public void setC_by(Long c_by) {
		this.c_by = c_by;
	}

	public LocalDateTime getC_on() {
		return c_on;
	}

	public void setC_on(LocalDateTime c_on) {
		this.c_on = c_on;
	}

	public Long getM_by() {
		return m_by;
	}

	public void setM_by(Long m_by) {
		this.m_by = m_by;
	}

	public LocalDateTime getM_on() {
		return m_on;
	}

	public void setM_on(LocalDateTime m_on) {
		this.m_on = m_on;
	}

	public Long getD_by() {
		return d_by;
	}

	public void setD_by(Long d_by) {
		this.d_by = d_by;
	}

	public LocalDateTime getD_on() {
		return d_on;
	}

	public void setD_on(LocalDateTime d_on) {
		this.d_on = d_on;
	}

	public Boolean getIs_del() {
		return is_del;
	}

	public void setIs_del(Boolean is_del) {
		this.is_del = is_del;
	}

}
