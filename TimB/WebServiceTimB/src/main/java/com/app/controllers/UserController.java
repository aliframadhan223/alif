package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserController {
	@GetMapping("usermanagement")
	public String user() {
		return "usermanagement/usermanagement";
	}
	
	@GetMapping("adduser")
	public String adduser() {
		return "usermanagement/add";
	}
	
	@GetMapping("updateuser")
	public String updateuser() {
		return "usermanagement/update";
	}
	

	@GetMapping("deleteuser")
	public String deleteuser() {
		return "usermanagement/delete";
	}


}
