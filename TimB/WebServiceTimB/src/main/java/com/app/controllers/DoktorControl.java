package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DoktorControl {

	@GetMapping("profiledokter")
	public String viewprofiledokter() {
		return "/profiledokter/profiledokter";
	}
	
	@GetMapping("tindakandokter")
	public String tindakandokter() {
		return "/profiledokter/tindakandokter";
	}
	

	@GetMapping("spesialisasi")
	public String spesialisasi() {
		return "/profiledokter/spesialisasidokter";
	}
	
	@GetMapping("tambahtindakan")
	public String tambahtindakandokter() {
		return "/profiledokter/tambahtindakandokter";
	}
	
	@GetMapping("hapustindakan")
	public String hapustindakandokter() {
		return "/profiledokter/hapustindakan";
	}
}
