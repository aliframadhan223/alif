package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class pasienCtrl {

	@GetMapping("pasien")
	public String pasien() {
		return "pasien/pasien";
	}
	
	@GetMapping("addpasien")
	public String addpasien() {
		return "pasien/addpasien";
	}
	
	@GetMapping("editpasien")
	public String editpasien() {
		return "pasien/editpasien";
	}
}
