
populateSelect()

function populateSelect() {
	$.ajax({
		url: "http://localhost:81/api/levellokasi/list/",
		type: "GET",
		success: function(hasil) {
			for (var i = 0; i < hasil.length; i++) {
				$('#sbLevel').append($('<option>', {
					text: hasil[i].name
				}));
			}
		}
	})


	$.ajax({
		url: "http://localhost:81/api/lokasi/list/",
		type: "GET",
		success: function(hasil) {
			for (var i = 0; i < hasil.length; i++) {
				if(hasil[i].namadaerahatas != 'Kosong'){
				$('#sbParent').append($('<option>', {
					text: hasil[i].namadaerahatas
				}));					
				}
			}
		}
	})
}

levlokById()

function levlokById() {
	let id = sessionStorage.getItem("idL")
	$.ajax({
		url: "http://localhost:81/api/lokasi/listByID/" + id,
		type: "GET",
		success: function(hasil) {

			$("#iLocName").val(hasil[0].namadaerah)
			$("#iParentName").val(hasil[0].namadaerahatas)


			$("#btnSimpanEdit").click(function() {
				var obj = {}
				obj.id = hasil.id
				obj.name = $("#iLocName").val()
				obj.parentId = $("#iParentName").val()
				obj.locationLevelId = $("#iParentName").val()
				obj.c_by = hasil.c_by
				obj.c_on = hasil.c_on
				obj.m_by = 2
				obj.m_on = hasil.m_on
				obj.d_by = hasil.d_by
				obj.d_on = hasil.d_on
				obj.is_del = hasil.is_del
				var myJson = JSON.stringify(obj)

				$.ajax({
					url: "http://localhost:81/api/levellokasi/update",
					type: "PUT",
					contentType: "application/json",
					data: myJson,
					success: function(hasil) {
						alert("Edit berhasil")
						$("#modalVariants").modal('hide')

						list()
					}
				})
			})
		}
	})
}

