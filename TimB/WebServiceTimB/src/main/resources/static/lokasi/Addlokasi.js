
populateSelect()

function populateSelect() {
	$.ajax({
		url: "http://localhost:81/api/levellokasi/list/",
		type: "GET",
		success: function(hasil) {
			for (var i = 0; i < hasil.length; i++) {
				$('#sbLevel').append($('<option>', {
					text: hasil[i].name,
					name: hasil[i].id
				}));
			}
		}
	})

	$.ajax({
		url: "http://localhost:81/api/lokasi/list/",
		type: "GET",
		success: function(hasil) {
			for (var i = 0; i < hasil.length; i++) {
				if (hasil[i].namadaerah != 'Kosong') {
					$('#sbParent').append($('<option>', {
						text: hasil[i].namadaerah,
						name: hasil[i].id
					}));
				}
			}
		}
	})
}

levlokById()

function levlokById() {
	let id = sessionStorage.getItem("idL")
	$.ajax({
		url: "http://localhost:81/api/lokasi/listByID/" + id,
		type: "GET",
		success: function(hasil) {
			console.log(hasil)

			$("#btnSimpanEdit").click(function() {
				var obj = {}
				obj.id = sessionStorage.getItem("iLastL")
				obj.locationLevelId = $('#sbLevel').find(":selected").attr("name")
				obj.parentId = $('#sbParent').find(":selected").attr("name")
				obj.name = $('#iLocName').val();
				obj.c_by = 1

				var myJson = JSON.stringify(obj)

				console.log(myJson)

				$.ajax({
					url: "http://localhost:81/api/lokasi/add",
					type: "POST",
					contentType: "application/json",
					data: myJson,
					success: function(hasil) {
						alert("Tambah berhasil")
						$("#modalVariants").modal('hide')

						location.reload()
					}
				})
				/*				var obj = {}
								obj.id = hasil.id
								obj.name = $("#iLocName").val()
								obj.parentId = $("#iParentName").val()
								obj.locationLevelId = $("#iParentName").val()
								obj.c_by = hasil.c_by
								obj.c_on = hasil.c_on
								obj.m_by = 2
								obj.m_on = hasil.m_on
								obj.d_by = hasil.d_by
								obj.d_on = hasil.d_on
								obj.is_del = hasil.is_del
								var myJson = JSON.stringify(obj)
				
								$.ajax({
									url: "http://localhost:81/api/lokasi/add",
									type: "PUT",
									contentType: "application/json",
									data: myJson,
									success: function(hasil) {
										alert("Edit berhasil")
										$("#modalVariants").modal('hide')
				
										list()
									}
								})*/
			})
		}
	})
}

