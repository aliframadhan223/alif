$(document).ready(function(){
	
	
	$("#emailHelp").hide()
	$("#emailemptyHelp").hide()
	$("#emailwrongHelp").hide()
	//validasi penulisan email
	function IsEmail(email) {
  		var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  		if(!regex.test(email)) {
    		return false;
  		}else{
    		return true;
  		}
  	}
	
	//check email
	let email = ""
	$("#btnemail").click(function(){
		email = $("#formEmail").val();
		if(email==""){
				$("#emailemptyHelp").show()
				$("#emailHelp").hide()
				$("#emailwrongHelp").hide()
		}else{
			var mail=IsEmail(email)
			if(mail==false){
				$("#emailHelp").hide()
				$("#emailemptyHelp").hide()
				$("#emailwrongHelp").show()
			}else{
			   sessionStorage.setItem("email", email)
				$.ajax({
					url		: "http://localhost:81/api/user/cekemail/"+email,
					type	: "GET",
					success	: function(hasil){
						if(hasil!=0){
							$("#emailHelp").show()
							$("#emailemptyHelp").hide()
							$("#emailwrongHelp").hide()
						}else{
							buattoken(email)
							
						}
					}
				})
			}
		}
	})
	


	function buattoken(email){
		newtoken()	
		
	}

	
	function newtoken(){
		//buat token
		var obj={}
		obj.email = email
		var myJson = JSON.stringify(obj)
		$.ajax({
			url			: "http://localhost:81/api/token/post",
			type		: "POST",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
				getoken(email)
			}
		})
		
}

	function getoken(email) {
		$.ajax({
			url: "http://localhost:81/api/token/gettoken/" + email,
			type: "GET",
			success: function(hasil) {
				sessionStorage.setItem("idtoken", hasil)
				$.ajax({
							url		: "veritifikasi",
							type	: "GET",
							dataType: "html",
							success	: function(hasil){
								$(".content").html(hasil)
							}
						})
							return false 
			}
		})
	}
$("TokenHelp").hide()
$("TokenemptyHelp").hide()


$("#btnotp").click(function(){
	
	let token= $("#itoken").val()
	let idtoken = sessionStorage.getItem("idtoken")
	cekvalid(token)
	
	})

	$("#btnresendotp").hide()
	$("#returntoemailver").hide()


function cekvalid(token){
	let idtoken = sessionStorage.getItem("idtoken")
	$.ajax({
				url		: "http://localhost:81/api/token/cekvalid/"+idtoken,
				type	: "GET",
				success	: function(hasil){
					console.log(idtoken)
					if(hasil<=0){
					$("TokenHelp").show()
					$("TokenemptyHelp").hide()
					}else{
						cektoken(token)
					}
				}
			})
}

startcountdown()
function startcountdown(){
	const intervalID =  setInterval(loopcount,1000)
	
	$("#returntoemailver").click(function(){
		$.ajax({
					url		: "daftaremail",
					type	: "GET",
					dataType: "html",
					success	: function(hasil){
								$(".content").html(hasil)
					}
				})
		})
		
		//kirim ulang otp
		$("#btnresendotp").click(function(){
			minute = 0
			second = 60
			//clearInterval(intervalID)
			$("#count").show()
		})
		
		
		var minute = 0
 		var second = 60
 		
 		function loopcount(){
			
	 		if(minute==0 && second == 1){
				 $("#returntoemailver").show()
				 $("#btnresendotp").show()
				 $("#count").hide()
				//clearInterval(intervalID) 
		 		//document.getElementById("count").innerHTML = "00:00";
	 		}else{
		 		second--;
		 		if(second==0){
			 		minute--;
			 		second=60;
			 		if(minute==0){
				 		minute=minute;
			 		}
		 		}
		 		if(minute.toString().length == 1){
					 minute = "0"+minute;
				 }
				 if(second.toString().length == 1){
					 second = "0"+second;
				 }
		 		document.getElementById("count").innerHTML = "Kirim ulang kode OTP dalam "+minute+":"+second;
	 		}
 		}
	}	
	
	function deactiveotplama(){
		let idtoken = sessionStorage.getItem("idtoken")
		let email	= sessionStorage.getItem("email")
		$.ajax({
			url			: "http://localhost:81/api/token/setexpired/"+idtoken,
			type		: "PUT",
			success		: function(hasil){
			}
		})
	}
	
	//kirim ulang otp
		$("#btnresendotp").click(function(){
			let email	= sessionStorage.getItem("email")
			deactiveotplama()
			buattoken2(email)
		})
		
	function buattoken2(email){
		newtoken2()	
	
	}
	
	function newtoken2(){
		//buat token
		let email	= sessionStorage.getItem("email")
		var obj={}
		obj.email = email
		var myJson = JSON.stringify(obj)
		$.ajax({
			url			: "http://localhost:81/api/token/post",
			type		: "POST",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
				getoken(email)
			}
		})
		
}
		function cektoken(token){
		let idtoken = sessionStorage.getItem("idtoken")
		$.ajax({
				url		: "http://localhost:81/api/token/cektoken/"+idtoken+"/"+token,
				type	: "GET",
				success	: function(hasil){
					console.log(hasil)
					if(hasil<=0){
						$("TokenemptyHelp").show()
						
					}else{
						deactiveotplama()
						$.ajax({
								url		: "penpassword",
								type	: "GET",
								dataType: "html",
								success	: function(hasil){
								$(".content").html(hasil)
							}
						})
					}
				}
			})
	}
	
	let password = "";
	let password2 = "";
	$("#passHelp").hide()
	$("#passemptyHelp").hide()
	$("#passwrongHelp").hide()
	$("#pass2wrongHelp").hide()
	$("#setPass").click(function(){
		password = $("#iPass1").val();
		password2 = $("#iPass2").val();
		cekpassword(password, password2)
		sessionStorage.setItem("iPass1", password)
		
	})
	
	
	
		var lowerCaseLetters = /[a-z]/g;
		var upperCaseLetters = /[A-Z]/g;
		var numbers = /[0-9]/g;
		var numBoolean=false
    	var specialChars = "!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?`~";
    	
	function cekpassword(password, password2){
		for (let i = 0; i < specialChars.length; i++) {
            for (let j = 0; j < password.length; j++) {
                if (specialChars[i] == password[j]) {
                    numBoolean = true;
                }
            }
        }
        if(password.length==0){
			$("#passemptyHelp").show()
			$("#passwrongHelp").hide()
			$("#pass2wrongHelp").hide()
			$("#passHelp").hide()
		}else if(!(password.match(lowerCaseLetters))>0 || !(password.match(upperCaseLetters))>0 || !(password.match(numbers))>0 || numBoolean==false || password.length<8){
			$("#passHelp").show()
			$("#passemptyHelp").hide()
			$("#passwrongHelp").hide()
			$("#pass2wrongHelp").hide()
		}else if(password2.length==0){
			$("#passwrongHelp").show()
			$("#pass2wrongHelp").hide()
			$("#passHelp").show()
			$("#passemptyHelp").hide()
		}else if(password!=password2){
			$("#pass2wrongHelp").show()
			$("#passHelp").show()
			$("#passemptyHelp").hide()
			$("#passwrongHelp").hide()
		}else{
			window.location.href = "/daftarbio" 
		}
	}
	
	
		//put option for role
	optionrole()
	function optionrole(){
		$.ajax({
			url			: "http://localhost:81/api/role/list",
			type		: "GET",
			success		: function(hasil){
				for(i=0;i<hasil.length;i++){
					$("#irole").append("<option value='"+hasil[i].id+"'>"+hasil[i].name+"</option>")
				}
			}
		})
	}
	
	$("#alertRl").hide()
	$("#alertRllocked").hide()
	$("#NameHelp").hide()
	$("#TelpHelp").hide()
	$("#btndaftar").click(function(){
			
		var role = $("#irole").val()
		var hp = $("#iPhone").val()
		var nama =   $("#iName").val()
		if(role==4){
				$("#alertRllocked").show()
		}else if(hp=="" || hp==undefined){
			createbiodata()
		}else{			
			var numhp=false
			for (let i = 0; i < specialChars.length; i++) {
            	for (let j = 0; j < hp.length; j++) {
             	   if (specialChars[i] == hp[j]) {
              	      numhp = true;
             	   }
           	 	}
        	}
        	
			if(hp.match(lowerCaseLetters) || hp.match(upperCaseLetters) || numhp==true || hp.length<8 || hp.length>13){
				$("#TelpHelp").show()
			}else if(role==4){
				$("#alertRllocked").show()
			}else{
				createbiodata()
			}
		}

	})
	
	
	//create biodata
	let nama= ""
	function createbiodata(){
		nama = $("#iName").val()
		if(nama == ""){
			$("#NameHelp").show()
		}else{
			var obj={}
			obj.fullname = nama
			obj.mobile_phone =  $("#iPhone").val()
			var myJson = JSON.stringify(obj)
		$.ajax({
			url			: "http://localhost:81/api/biodata/post",
			type		: "POST",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
			
				getbiodataid()
			}
		})
			
	}	
}
	
	//getting biodataid
	
	function getbiodataid(){
		var biodataid=0
		$.ajax({
		url			: "http://localhost:81/api/biodata/idbaru",
		type		: "GET",
		success		: function(hasil){
			biodataid=hasil
			createuser(biodataid)
		}
	})
	}
	
	function createuser(biodataid){

		var obj={}
		let email	= sessionStorage.getItem("email")
		let passwordfinal =sessionStorage.getItem("iPass1")
		obj.biodataid = biodataid
		obj.roleid =  $("#irole").val()
		obj.email = email
		obj.password = passwordfinal
		var myJson = JSON.stringify(obj)
		
		$.ajax({
			url			: "http://localhost:81/api/user/post",
			type		: "POST",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
				createrole(biodataid)
			}
		})
	}
	
		function createrole(biodataid){
		var obj={}		
		var opsi =  $("#irole").val()
		if(opsi==1){
			obj.biodataid = biodataid
			var myJson = JSON.stringify(obj)
			$.ajax({
			url			: "http://localhost:81/api/admin/post",
			type		: "POST",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
				window.location.href = "/" 
			}
		})
		}else if(opsi==2){
			obj.parent_biodata_id = biodataid
			var myJson = JSON.stringify(obj)
			$.ajax({
			url			: "http://localhost:81/api/customer_member/post",
			type		: "POST",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
			window.location.href = "/" 
			}
		})
		}else if(opsi==3){
			obj.biodataid = biodataid
			var myJson = JSON.stringify(obj)
			$.ajax({
			url			: "http://localhost:81/api/doctor/post",
			type		: "POST",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
				window.location.href = "/" 
			}
		})
		}
		
	}
	




})