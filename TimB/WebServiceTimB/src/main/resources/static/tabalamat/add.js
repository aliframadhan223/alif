$(document).ready(function() {
	addlocation()
	function addlocation() {
		$.ajax({

			url: "http://localhost:81/api/tabalamat/listkotakec",
			type: "GET",
			success: function(hasil) {
				for (i = 0; i < hasil.length; i++) {
					$("#ilocationid").append("<option value='" + hasil[i].id + "'>" + hasil[i].keckota + "</option>")
				}

			}
		})
	}

	for (i = 0; i <= 5; i++) {
		$(".eror" + i + "").hide()
	}

	let eror0=0, eror1 = 0, eror2 = 0, eror3 = 0, eror4 = 0, eror5 = 0
	let biodataid = sessionStorage.getItem("biodataid")
	$(".btnsimpan").click(function() {
		let lbl = $("#ilabel").val()
		let label = lbl.toLowerCase()
		let valid1 = lbl.replace(/ /, "")
		let valid2 = $("#irecipient").val().replace(/ /g, "")
		let valid3 = $("#irecipientphonenumber").val().replace(/ /g, "")
		let valnum = valid3.replace(/[0-9]/g, "")
		let valid5 = $("#iaddress").val().replace(/ /g, "")

		if (valid1.length == 0) {
			$(".eror1").show()
			eror1 += 1
		} else {
			$(".eror1").hide()
			eror1 = 0
		}

		if (valid2.length == 0) {
			$(".eror2").show()
			eror2 += 1
		} else {
			$(".eror2").hide()
			eror2 = 0
		}

		if (valid3.length == 0) {
			$(".eror3").show()
			eror3 += 1
		} else {
			$(".eror3").hide()
			eror3 = 0
			if (valnum != 0) {
				$(".eror6").show()
				eror6 += 1
			} else {
				$(".eror6").hide()
				eror6 = 0
			}
		}

		if ($("#ilocationid").val().length == 0) {
			$(".eror4").show()
			eror4 += 1
		} else {
			$(".eror4").hide()
			eror4 = 0
		}

		if (valid5.length == 0) {
			$(".eror5").show()
			eror5 += 1
		} else {
			$(".eror5").hide()
			eror5 = 0
		}

		if (eror1 == 0 && eror2 == 0 && eror3 == 0 && eror4 == 0 && eror5 == 0) {
			$.ajax({
				url: "http://localhost:81/api/tabalamat/searchvalidasi/" + biodataid + "/" + label,
				type: "GET",
				contentType: "application/json",
				success: function(hasil) {
					console.log(hasil)
					if (hasil.length != 0) {
						eror0 += 1
						$(".eror0").show()
					} else {
						eror0 = 0
						$(".pesan0").hide()
						var obj = {}
						obj.biodataid = biodataid
						obj.label = $("#ilabel").val()
						obj.recipient = $("#irecipient").val()
						obj.recipientphonenumber = $("#irecipientphonenumber").val()
						obj.locationid = $("#ilocationid").val()
						obj.postalcode = $("#ipostalcode").val()
						obj.address = $("#iaddress").val()

						var myJson = JSON.stringify(obj)
						$.ajax({
							url: "http://localhost:81/api/tabalamat/add",
							type: "POST",
							contentType: "application/json",
							data: myJson,
							success: function() {
								$("#modaltabalamat").modal('hide') //show modal terlebih dahulu
								alert("simpan Berhasil")
								kembali()
							}
						})
					}
				}
			})
		} else {
			alert("Alamat tidak tersimpan!")
			return false
		}
	})

	function kembali() {
		$.ajax({
			url: "http://localhost/viewtabalamat",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$("#modaltabalamat").modal('hide')
				$(".isimain").html(hasil)
			}
		})
	}


	$(".btnbatal").click(function() {
		$("#modaltabalamat").modal('hide')
	})


})