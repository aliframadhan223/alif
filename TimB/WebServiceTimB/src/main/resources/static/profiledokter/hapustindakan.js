$(document).ready(function() {

	$(".btnbatal").click(function() {
		$("#modaltindakandokter").modal("hide")
	})

	let id = sessionStorage.getItem("treatmentid")

	tindakan()
	function tindakan() {
		$.ajax({
			url: "http://localhost:81/api/tindakandokter/tindakanid/" + id,
			type: "GET",
			success: function(hasil) {
				console.log(hasil)
				$("#hapustindakan").text(hasil.name)
			}
		})
	}

	$(".btnhapus").click(function() {
		var obj = {}
		obj.id = id
		var myjson = JSON.stringify(obj)
		$.ajax({
			url: "http://localhost:81/api/tindakandokter/deletetindakan",
			type: "DELETE",
			contentType: "application/json",
			data: myjson,
			success: function() {
				alert("Delete Berhasil")
				kembali()
			},
		})
	})

	function kembali() {
		$.ajax({
			url: "http://localhost/tindakandokter",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$("#modaltindakandokter").modal('hide')
				$(".isimaindokter").html(hasil)
			}
		})
	}
})