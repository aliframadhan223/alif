$(document).ready(function() {
	let doctorid = sessionStorage.getItem("biodataid")

	$(".eror1").hide()
	$(".eror2").hide()

	$(".btnbatal").click(function() {
		kembali()
	})

	$(".btnsimpan").click(function() {
		let tdokter = $(".itindakan").val()
		let tindakan = tdokter.toLowerCase()
		let valid = tdokter.replace(/ /g, "")

		if (valid.length == 0) {
			$(".eror1").show()
		} else {
			$(".eror1").hide()
			$.ajax({
				url: "http://localhost:81/api/tindakandokter/searchvalidasi/" + doctorid + "/" + tindakan,
				type: "GET",
				contentType: "application/json",
				success: function(hasil) {
					console.log(hasil)
					if (hasil.length != 0) {
						$(".eror2").show()
					} else {
						$(".eror2").hide()
						var obj = {}
						obj.doctorId = doctorid
						obj.name = $(".itindakan").val()
						var myJson = JSON.stringify(obj)
						$.ajax({
							url: "http://localhost:81/api/tindakandokter/add",
							type: "POST",
							contentType: "application/json",
							data: myJson,
							success: function(hasil) {
								console.log(hasil)
								alert("Tindakan berhasil disimpan")
								kembali()
							}
						})
					}
				}
			})
		}
	})

	function kembali() {
		$.ajax({
			url: "http://localhost/tindakandokter",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$("#modaltindakandokter").modal('hide')
				$(".isimaindokter").html(hasil)
			}
		})
	}
})