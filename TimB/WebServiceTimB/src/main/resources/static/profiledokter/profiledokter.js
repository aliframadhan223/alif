$(document).ready(function() {

	let iddoctor = sessionStorage.getItem("biodataid")

	tampilnamadokter()
	function tampilnamadokter() {
		
		$.ajax({
			url: "http://localhost:81/api/tindakandokter/tindakandokterbyid/" + iddoctor,
			type: "GET",
			success: function(hasil) {
				$("#nama").text(hasil[0].namadokter)
				console.log(hasil)
			}
		})
	}

	tampiltindakan()
	function tampiltindakan() {
	
		$.ajax({
			url: "http://localhost:81/api/tindakandokter/tindakandokterbyid/" + iddoctor,
			type: "GET",
			success: function(hasil) {
				console.log(hasil)
				let txt = ""
				for (i = 0; i < hasil.length; i++) {
					txt += "<span class='justify-content-between align-items-center mx-3 my-9' style='word-spacing: normal'>- <tr>"
					txt += "<td>" + hasil[i].name + "</td>"
					txt += "</tr></span>"
				}
				$(".tindakandokter").append(txt)
			}
		})
		return false
	}

	$(".tabtindakan").click(function() {
		let coba = $(this).attr("title")
		$.ajax({
			url: coba,
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$(".isimaindokter").html(hasil)
			}
		})
		return false
	})
	$(".tabspesialisasi").click(function() {
		let coba = $(this).attr("title")
		$.ajax({
			url: coba,
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$(".isimaindokter").html(hasil)
			}
		})
		return false
	})
})
