//alert("haii")

$(document).ready(function() {

	$(".btnAdd").click(function() {
		$.ajax({
			url: "http://localhost/adduser",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$("#ModalUserManagement").modal('show')//modal show
				$(".isiModalUserManagement").html(hasil)

			}
		})
		return false
	})

	function kembali() {
		$.ajax({
			url: "http://localhost/usermanagement",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$(".isiMain").html(hasil)
			}
		})
	}
	$("#inUserManagementCari").on('input', function() {
		let cari = $(this).val()
		list(cari)
	})

	list()

	function list(isi) {
		
		let alamat = ""
		if (isi == undefined || isi == "") {
			alamat = "http://localhost:81/api/user/list1"
		} else {
			alamat = "http://localhost:81/api/user/listnama/" + isi
		}
		$.ajax({
			url: alamat,
			type: "GET",
			success: function(hasil) {
				//alert("okeey")
					let txt = ""
				for (i = 0; i < hasil.length; i++) {
					txt += "<tr>"
					txt += "<td>" + (i+1) + " </td>"
					txt += "<td>" + hasil[i].biodata_id + " </td>"
					txt += "<td>" + hasil[i].role_id + " </td>"
					txt += "<td>" + hasil[i].email + " </td>"
					txt += "<td>" + hasil[i].password + " </td>"
					txt += "<td>" + hasil[i].is_locked + " </td>"
					txt += "<td>" + hasil[i].last_login + " </td>"
					txt += "<td><button class='btn btn-info btnUpdate' name='" + hasil[i].id + "'><i class='fa-solid fa-pen-to-square'></i>edit</button> <button class='btn btn-danger btnDelete' name='" + hasil[i].id + "'><i class='fa-solid fa-trash-can'></i> hapus</button> </td>"
					txt += "</tr>"
					}
					$(".tb1").empty()
					$(".tb1").append(txt)
					$(".btnUpdate").click(function() {
						let id = $(this).attr("name");
						sessionStorage.setItem("id", id)
						$.ajax({
							url: "http://localhost/updateuser",
							type: "GET",
							datatype: "html",
							success: function(hasil) {
								$("#ModalUserManagement").modal('show')
							$(".isiModalUserManagement").html(hasil)
							}
						})
						return false

						//alert("hapus")

						//window.open("/updateproduct")
					})
					$(".btnDelete").click(function() {
						let id = $(this).attr("name");
						sessionStorage.setItem("id", id)
						swal({
							title: "Are you sure?",
							text: "Data Will Deleted",
							icon: "warning",
							buttons: true,
							dangerMode: true,
						})
							.then((willDelete) => {
								if (willDelete) {
									let id = sessionStorage.getItem("id") //kalau diclear maka session storage nya hilang
									var obj = {}
									obj.id = id
									var myJson = JSON.stringify(obj)
									$.ajax({
										url: "http://localhost:81/api/user/delete",
										type: "DELETE",
										contentType: "application/json",
										data: myJson,
										success: function(hasil) {
											swal({
												title: "Success!",
												text: "Delete Success!",
												icon: "success",
												buttons: true,
											});
											kembali()
											//window.open("/product")

										}

									})
								} else {

								}
							});


						//	window.open("/delete")
					})
					//$(".tb1").append("<tr> <td>1</td> <td>2</td> <td>3</td> </tr>")
				}
			
		})
	}
$(".btnKembali").click(function() {
	$.ajax({
		url: "http://localhost/usermanagement",
		type: "GET",
		datatype: "html",
		success: function(hasil) {
			$("#ModalUserManagement").modal('hide')
		}
	})
	return false
})

	$(".btnRefresh").click(function() {
		list()
	})
})