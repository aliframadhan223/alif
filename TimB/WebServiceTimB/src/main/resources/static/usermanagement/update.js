

userById() //untuk memanggil function

//buat function untuk get by id
function userById() {
	let id = sessionStorage.getItem("id") //kalau diclear maka session storage nya hilang
	$.ajax({
		url: "http://localhost:81/api/user/list/" + id,
		type: "GET",
		success: function(hasil) {
			$("#iId").val(hasil.id)
			obj.role = $("#iRole").val()
			obj.email = $("#iEmail").val()
			obj.password = $("#iPassword").val()
			obj.is_locked = $("#iLocked").val()
		}

	})
}

$(document).ready(function() {
	function kembali() {
		$.ajax({
			url: "http://localhost/usermanagement",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$(".isiMain").html(hasil)
			}
		})
	}
	$(".btnKembali").click(function() {
		$.ajax({
			url: "http://localhost/usermanagement",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$("#ModalUserManagement").modal('hide')
			}
		})
		return false
	})

	$(".bt-update").click(function() {
		let id = sessionStorage.getItem("id") //kalau diclear maka session storage nya hilang
		var obj = {}
		obj.id = id
		obj.role = $("#iRole").val()
		obj.email = $("#iEmail").val()
		obj.password = $("#iPassword").val()
		obj.is_locked = $("#iLocked").val()
		var myJson = JSON.stringify(obj)
		$.ajax({
			url: "http://localhost:81/api/user/update",
			type: "PUT",
			contentType: "application/json",
			data: myJson,
			success: function(hasil) {
				$("#ModalUserManagement").modal('hide')
				swal({
					title: "Success!",
					text: "Update Success!",
					icon: "success",
					buttons: true,
				});
				kembali()
				//window.open("/product")

			}

		})
	})
})