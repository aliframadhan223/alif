$(document).ready(function() {

	$("#btntmbh").click(function() {
		$.ajax({
			url: "http://localhost/addblood",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$("#modalBlood").modal('show')
				$(".isiModalBlood").html(hasil)
			}
		})
		return false
	})

	$("#inBloodCari").keyup(function() {
		let cari = $(this).val()
		list(cari)
	})
	list()

	function list(isi) {
		let alamat = ""
		if (isi == undefined || isi == "" || isi== " ") {
			alamat = "http://localhost:81/api/blood/list"
		}
		else {
			alamat = "http://localhost:81/api/blood/listbyname/" + isi
		}

		$.ajax({
			url: alamat,
			type: "GET",
			success: function(hasil) {
				let txt = ""
				let no=1
				for (i = 0; i < hasil.length; i++) {
					txt += `<tr >`
					txt+= `<td>${no}</td>`
					txt += `<td>${hasil[i].code}</td>`
					txt += `<td>${hasil[i].description}</td>`
					txt += "<td><button class='btn btn-info btnEdit' name='" + hasil[i].id + "' ><i class='fas fa-pen-to-square'></i>edit</button>  <button class='btn btn-danger btndelete' name='" + hasil[i].id + "' ><i class='fas fa-trash'></i>hapus</button></td>"
					txt += `</tr>`
					no++
				}
				$("#tb2").empty()
				$("#tb2").append(txt)

				$(".btnEdit").click(function() {
					let id = $(this).attr("name")
					sessionStorage.setItem("id", id)

					$.ajax({
						url: "http://localhost/editblood",
						type: "GET",
						dataType: "html",
						success: function(hasil) {
							$("#modalBlood").modal('show')
							$(".isiModalBlood").html(hasil)
						}
					})
					return false

				})

				$(".btndelete").click(function() {
					let idod = $(this).attr("name")
					sessionStorage.setItem("id", idod)
					var obj = {}
					obj.id = idod
					var myJson = JSON.stringify(obj)

					swal({
						title: "Are you sure?",
						text: "Data Will be Deleted",
						icon: "warning",
						buttons: true,
						dangerMode: true,
					})

						.then((willDelete) => {
							if (willDelete) {
								$.ajax({
									url: "http://localhost:81/api/blood/delete",
									type: "DELETE",
									contentType: "application/json",
									data: myJson,
									success: function(hasil) {
										swal("Delete Berhasil", {
											icon: "success",
										});
										
										list()
									}
								})

							} else {
							}
						});


					return false
				})



			}

		})


	}

	list()

})