$(document).ready(function() {

	apibaseserver = "http://localhost:81/"
	apibaseurl = "api/levellokasi/"

	$("#btnAdd").click(function() {
		$.ajax({
			url: "http://localhost/lokasileveladd",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$("#modalVariants").modal('show')
				$(".isiModalVariants").html(hasil)
			}
		})
		return false
	})

	$("#btnSearch").click(function() {
		list($("#txtCari").val())
	})

	list()

	function list(val) {

		let listurl = apibaseserver + apibaseurl + "list"

		if (val != undefined && val != "") {
			listurl = listurl + "/" + val
		}

		$.ajax({
			url: listurl,
			type: "GET",
			success: function(listhasil) {
				let txt = ""
				for (i = 0; i < listhasil.length; i++) {
					txt += "<tr>"
					txt += "<td>" + listhasil[i].name + "</td>"
					txt += "<td>" + listhasil[i].abbr + "</td>"
					txt += "<td> <input type='button' value='Edit' class='btnEdit btn btn-primary' name='" + listhasil[i].id + "'> "
					txt += "<input type='button' value='Delete' class='btnDelete btn btn-danger' name='" + listhasil[i].id + "'> </td>"
					txt += "</tr>"
					sessionStorage.setItem("id", listhasil[i].id)
				}

				$('#tbll').empty();
				$('#tbll').append(txt);

				$('#myTable').DataTable({
					searching: false,
					info: false,
					retrieve: true,
					columns: [
						{ title: 'Nama' },
						{ title: 'Nama Singkatan' },
						{ title: '#' },
					]
				});

				/*$("#tbll").empty();
				$("#tbll").append(txt)*/

				$(".btnEdit").click(function() {
					let id = $(this).attr("name");
					sessionStorage.setItem("id", id);
					$.ajax({
						url: "http://localhost/lokasileveledit",
						type: "GET",
						dataType: "html",
						success: function(hasil) {
							$("#modalVariants").modal('show')
							$(".isiModalVariants").html(hasil)
						}
					})
					return false
				})

				$(".btnDelete").click(function() {
					let id = $(this).attr("name");

					$.ajax({
						url: apibaseserver + apibaseurl + "countlok/" + id,
						type: "GET",
						success: function(hasildel) {
							if (hasildel > 0) {
								alert("Tidak dapat menghapus level lokasi '"+ listhasil[id-1].name+"' karena masih digunakan")
							} else {
								$.ajax({
									url: apibaseserver + apibaseurl + "LevLokById/" + id,
									type: "GET",
									success: function(hasil) {
										var obj = hasil
										obj.is_del = true
										var myJson = JSON.stringify(obj)

										console.log(myJson)

										if (confirm("Apa anda yakin ingin menghapus data '"+listhasil[id-1].name+"'?")) {
											$.ajax({
												url: apibaseserver + apibaseurl + "update",
												type: "PUT",
												contentType: "application/json",
												data: myJson,
												success: function(hasil) {
													alert("Hapus berhasil")
													location.reload()
												}
											})
										}
										return false;
									}
								})
							}

							return false;
						}
					})
				})
			}
		})
	}
})
