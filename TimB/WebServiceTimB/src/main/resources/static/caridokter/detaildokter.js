let urlserver = "http://localhost:81/api/doctor/"

isipage()


function isipage() {
	let id = sessionStorage.getItem("idDokter")

	url = urlserver + "headdoctordetail?idDoctor=" + id
	$.ajax({
		url: url,
		type: "GET",
		success: function(hasilhead) {
			console.log(hasilhead)

			$('#headName').text(hasilhead[0].fullname)
			$('#exp').text("Pengalaman " + hasilhead[0].pengalaman + " tahun")
		}
	})

	url = urlserver + "tindakanmedisdetail?idDoctor=" + id
	$.ajax({
		url: url,
		type: "GET",
		success: function(hasiltindakanmedis) {
			console.log(hasiltindakanmedis)

			let listtindakanmedis = ""

			$.each(hasiltindakanmedis, function(key, value) {
				listtindakanmedis += "<li class='mb-1'>" + value.name + "</li>"
			})

			$("#listtindakanmedis").append(listtindakanmedis)
		}
	})

	url = urlserver + "riwayatpraktekdetail?idDoctor=" + id
	$.ajax({
		url: url,
		type: "GET",
		success: function(hasilprakdetil) {
			console.log(hasilprakdetil)

			let listpraktik = ""


			$.each(hasilprakdetil, function(key, value) {

				let sampaitahun = value.deleted_on

				if (sampaitahun == null) {
					sampaitahun = "sekarang"
				}

				listpraktik += "<ul class='list-unstyled mb-0 text-sm text-muted'>"
				listpraktik += "<li class='mb-1'>" + value.namafaskes + "</li>"
				listpraktik += "<li class='mb-1 float-end'>" + value.tahunmasuk + "-" + sampaitahun + "</li>"
				listpraktik += "<li class='mb-1'>" + value.namaspesialisasi + "</li>"
				listpraktik += "</ul>"
			})

			$("#listriwayatdokter").append(listpraktik)
		}
	})

	url = urlserver + "jadwaldokterdetail?idDoctor=" + id
	$.ajax({
		url: url,
		type: "GET",
		success: function(hasiljadwal) {
			console.log(hasiljadwal)

			let listjadwal = ""

			$.each(hasiljadwal, function(key, value) {
				listjadwal += "<small class='text-muted'>" + value.day + " " + value.time_schedule_start + ":" + value.time_schedule_end + " " + "</small><br>"
			})

			$(".jadwaldokter").append(listjadwal)
		}
	})

	url = urlserver + "lokasipraktekdetail?idDoctor=" + id
	$.ajax({
		url: url,
		type: "GET",
		success: function(hasillokprak) {
			console.log(hasillokprak)

			let listlokprak = ""

			$.each(hasillokprak, function(key, value) {

				if (key != 0) {
					listlokprak += "<hr>"
				}

				listlokprak += "<div class='d-flex align-items-start'>"
				listlokprak += "<div class='flex-grow-1'>"
				listlokprak += "<small class='float-end text-navy'>Konsultasi mulai dari</small>"
				listlokprak += "<strong class='namafaskes'>" + value.namafaskes + "</strong><br>"
				listlokprak += "<small class='float-end text-navy'>xxx</small>"
				listlokprak += "<small class='text-muted catfaskes'>" + value.kategorifaskes + "</small><br>"
				listlokprak += "<small class='text-muted alamatfaskes'>" + value.location + "</small><br></div></div><br>"

				url = urlserver + "jadwaldokterdetail?idDoctor=" + id
				$.ajax({
					url: url,
					type: "GET",
					success: function(hasiljadwal) {
						let listjadwal = ""
						$.each(hasiljadwal, function(key, value) {
							listjadwal += "test " + value.time_schedule_start
							listjadwal += "test " + value.time_schedule_end
							listjadwal += "test " + value.day
							listlokprak += listjadwal
						})
					}
				})
			})

			console.log(listlokprak)
			$(".isilokprak").append(listlokprak)
		}
	})

	return false
}

